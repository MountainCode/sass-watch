# Sass Watch

Watches for changes to all .scss files in the same directory and recompiles to
a .css file with the same name as the base .scss file.

## Usage

```bash
sass-watch path/to/base.scss
```
