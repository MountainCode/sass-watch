{-# LANGUAGE OverloadedStrings #-}

module Main where

import Prelude hiding (FilePath)
import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Data.Maybe (fromMaybe)
import Data.Text (pack, unpack)
import Filesystem.Path.CurrentOS
import Lib
import Safe (headMay)
import System.Environment (getArgs)
import System.Exit (die)
import System.FSNotify

compileIfModified :: String -> Event -> IO ()
compileIfModified inPath e = do
  case e of (Modified _ _) -> compileToFile inPath e
            (Added _ _) -> return ()
            (Removed _ _) -> return ()

compileToFile :: String -> Event -> IO ()
compileToFile inPath e = do
  css <- compileSass inPath options
  let outPath = unpack $ case (toText (let p = fromText . pack $ inPath in (directory p) </> (basename p) <.> "css"))
                         of (Right p) -> p
                            (Left _) -> "out.css"
  writeFile outPath css
  putStrLn $ (eventPath e) ++ " changed. Recompiling " ++ inPath ++ " to " ++ outPath

isSassFile :: Event -> Bool
isSassFile e = fromMaybe False (fmap (== "scss") (extension . fromText . pack . eventPath $ e))

watch :: String -> String -> IO ()
watch dir inPath =
  withManager $ \mgr -> do
    watchDir mgr dir isSassFile (compileIfModified inPath)
    forever $ threadDelay 1000000

main = do
  args <- getArgs
  path <- case headMay args
          of Nothing -> die "Must include a file argument"
             Just v  -> return v
  dir <- case (toText . directory . fromText . pack $ path)
          of (Right t) -> return (unpack t)
             (Left m) -> die ("Could not convert " ++ (unpack m) ++ " to a FilePath")
  putStrLn "Watching for changes..."
  watch dir path
