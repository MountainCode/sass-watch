module Lib
    ( compileSass
    , options
    ) where

import Data.Maybe (fromMaybe)
import System.Exit (die)
import Text.Sass

compileSass :: FilePath -> SassOptions -> IO String
compileSass path opts = do
  result <- compileFile path options
  case result of Left e  -> (errorMessage e >>= die)
                 Right s -> return s
  
options = def { sassOutputStyle = SassStyleExpanded
              , sassOutputPath = Just "out.css"
              }
